/////////////////////////////////////////////////////////////////////////////
// FILE: _normalResidualsTests.tol
// PURPOSE: Declares NameBlok BysMcmc::Inference
/////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
NameBlock NormalResidualsTests =
//////////////////////////////////////////////////////////////////////////////
[[
Real frqShowNumSig = 3;

//////////////////////////////////////////////////////////////////////////////
Set CentralHistogram_F(Matrix frq, Real sigma, Real S)
//////////////////////////////////////////////////////////////////////////////
{
  Set For(1,Rows(frq),Set(Real k)
  {
    Real b = MatDat(frq,k,1);
    Real a = If(k>1,MatDat(frq,k-1,1), b - 2*(MatDat(frq,k+1,1)-b));
    Real numSig = (a+b)/2;
    Real frequence = MatDat(frq,k,2) / S;
    Real normal = DistNormal(b,0,sigma) - DistNormal(a,0,sigma);
    [[numSig,normal,frequence]]
  })
};

//////////////////////////////////////////////////////////////////////////////
Set CentralHistogram_S(Serie res, Real parts_)
//////////////////////////////////////////////////////////////////////////////
{
  Real S = CountS(res);
//WriteLn("TRACE [CentralHistogram_S] 1 parts_="<<parts_);
  Real parts = If(IsUnknown(parts_),Floor(Sqrt(S)),parts_);
//WriteLn("TRACE [CentralHistogram_S] 2 parts="<<parts);
  Real sigma = StDsS(res);
//WriteLn("TRACE [CentralHistogram_S] 3 sigma="<<sigma);
  Real show = frqShowNumSig*sigma;
//WriteLn("TRACE [CentralHistogram_S] 4 show="<<show);
  Matrix frq = SerFrequency(res,parts,-show,+show);
//WriteLn("TRACE [CentralHistogram_S] 5 frq:("<<Rows(frq)+"x"<<Columns(frq)+")");
  CentralHistogram_F(frq,sigma,S)
};
//////////////////////////////////////////////////////////////////////////////
Set CentralHistogram_M(Matrix res, Real parts_)
//////////////////////////////////////////////////////////////////////////////
{
  Real S = Rows(res);
  Real parts = If(IsUnknown(parts_),Floor(Sqrt(S)),parts_);
  Real sigma = MatStDs(res);
  Real show = frqShowNumSig*sigma;
  Matrix frq = Frequency(res,parts,-show,+show);
  CentralHistogram_F(frq,sigma,S)
};
//////////////////////////////////////////////////////////////////////////////
Set PartialCentralHistogram_S(Serie res, Real parts_, Real maxNumSig)
//////////////////////////////////////////////////////////////////////////////
{
  Real parts = If(IsUnknown(parts_),Floor(Sqrt(CountS(res))),parts_);
  Serie resNorm = res / StDsS(res);
  Serie resNormLowSig = IfSer(GT(Abs(resNorm),maxNumSig),?,resNorm);
  Real sigmaLow = StDsS(resNormLowSig);
  Real show = frqShowNumSig*sigmaLow;
  Matrix frq = SerFrequency(resNormLowSig,parts,-show,show);
  Real S = SumS(resNormLowSig*0+1);
  CentralHistogram_F(frq,sigmaLow,S)
};
//////////////////////////////////////////////////////////////////////////////
Set PartialCentralHistogram_M(Matrix res, Real parts_, Real maxNumSig)
//////////////////////////////////////////////////////////////////////////////
{
  Real parts = If(IsUnknown(parts_),Floor(Sqrt(CountS(res))),parts_);
  Matrix resNorm = res * 1/ MatStDs(res);
  Matrix resNormLowSig = IfMat(GT(Abs(resNorm),maxNumSig),?,resNorm);
  Real sigmaLow = MatStDs(resNormLowSig);
  Real show = frqShowNumSig*sigmaLow;
  Matrix frq = Frequency(resNormLowSig,parts,-show,show);
  Real S = MatSum(resNormLowSig*0+1);
  CentralHistogram_F(frq,sigmaLow,S)
};

//////////////////////////////////////////////////////////////////////////////
Set StandardDiagnostics(Anything res, Real numAcf, Real freeDegree)
//////////////////////////////////////////////////////////////////////////////
{
  [[
    Set normalPearsonChiSqrTest = NormalPearsonChiSqrTest(res)
  ]] << 
  If(!numAcf,Empty, 
  [[
    Set boxPierceLjungTest = BoxPierceLjungTest(res,numAcf,freeDegree),
    Set boxPierceModified  = BoxPierceModified (res,numAcf,freeDegree) 
  ]])
}

]];
  


